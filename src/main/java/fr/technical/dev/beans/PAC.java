package fr.technical.dev.beans;

import fr.technical.dev.enums.TypePac;

public class PAC {

	private String name;

	private Boolean over;

	private TypePac typePac;

	public PAC(String name, Boolean over, TypePac typePac) {
		super();
		this.typePac = typePac;
		this.over = over;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TypePac getTypePac() {
		return typePac;
	}

	public Boolean getOver() {
		return over;
	}

	public void setOver(Boolean over) {
		this.over = over;
	}

	public void setTypePac(TypePac typePac) {
		this.typePac = typePac;
		this.name = name;
	}




}
