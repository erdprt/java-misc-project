package fr.technical.dev.beans;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import fr.technical.dev.enums.TypePac;

public class Declaration {

	private List<PAC> pacs;

	private Date birthDate;

	public Declaration() {
		super();
	}

	public Declaration(Date birthDate) {
		super();
		this.birthDate = birthDate;
	}

	public Declaration(Date birthDate, List<PAC> pacs) {
		super();
		this.pacs = pacs;
		this.birthDate = birthDate;
	}

	public List<PAC> getPacs() {
		return pacs;
	}

	public void setPacs(List<PAC> pacs) {
		this.pacs = pacs;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}


	public List<PAC> getPacList(final TypePac typePac, final Boolean includes) {
		if (pacs!=null) {
			Predicate<PAC> predicate1 = pac -> includes && typePac.equals(pac.getTypePac());
			Predicate<PAC> predicate2 = pac -> !includes && !typePac.equals(pac.getTypePac());

			/*
			return pacs.stream().filter(pac -> !pac.getOver()).
									filter(pac ->  (includes && typePac.equals(pac.getTypePac())) ||
													(!includes && !typePac.equals(pac.getTypePac()))
														).
								collect(Collectors.toList());
			*/
			return pacs.stream().filter(pac -> !pac.getOver()).
								filter(predicate1.or(predicate2)).
				collect(Collectors.toList());

		}
		return null;
	}

	public Map<TypePac, List<PAC>> groupByTypePac() {
		if (pacs!=null) {
			return pacs.stream().filter(pac -> !pac.getOver()).
					collect(Collectors.groupingBy(PAC::getTypePac));

		}
		return null;
	}


}
