package fr.technical.dev.enums;

public enum TypePac {

	PAC_H("H"),
	PAC_I("I"),
	PAC_A("A");

	private TypePac(String type) {
		this.type = type;
	}

	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


}
