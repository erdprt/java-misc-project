package fr.technical.dev.beans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import fr.technical.dev.enums.TypePac;

public class DeclarationTest {

	private static final SimpleDateFormat DT = new SimpleDateFormat("yyyy-MM-dd");


	@Test
	public void getPacList() throws ParseException {

		Declaration declaration = createDeclaration(createDate("2017-01-23"),
												Arrays.asList(createPAC("pac1", true, TypePac.PAC_A),
																createPAC("pac2", false,  TypePac.PAC_H),
																createPAC("pac3", false,  TypePac.PAC_I),
																createPAC("pa4", false, TypePac.PAC_H),
																createPAC("pa5", true, TypePac.PAC_I),
																createPAC("pa6", false, TypePac.PAC_I),
																createPAC("pa7", false, TypePac.PAC_A)));

		List<PAC> pacs = declaration.getPacList(TypePac.PAC_H, true);
		Assert.assertEquals(2, pacs.size());

		pacs = declaration.getPacList(TypePac.PAC_A, true);
		Assert.assertEquals(1, pacs.size());


		pacs = declaration.getPacList(TypePac.PAC_H, false);
		Assert.assertEquals(3, pacs.size());

		pacs = declaration.getPacList(TypePac.PAC_A, false);
		Assert.assertEquals(4, pacs.size());

	}


	@Test
	public void groupByTypePac() throws ParseException {

		Declaration declaration = createDeclaration(createDate("2017-01-23"),
												Arrays.asList(createPAC("pac1", true, TypePac.PAC_A),
																createPAC("pac2", false,  TypePac.PAC_H),
																createPAC("pac3", false,  TypePac.PAC_I),
																createPAC("pa4", false, TypePac.PAC_H),
																createPAC("pa5", true, TypePac.PAC_I),
																createPAC("pa6", false, TypePac.PAC_I),
																createPAC("pa7", false, TypePac.PAC_A)));

		Map<TypePac, List<PAC>> pacs = declaration.groupByTypePac();
		Assert.assertEquals(1, pacs.get(TypePac.PAC_A).size());
		Assert.assertEquals(2, pacs.get(TypePac.PAC_H).size());
		Assert.assertEquals(2, pacs.get(TypePac.PAC_I).size());

	}


	public Declaration createDeclaration(final Date birthDate, List<PAC> pacs) {
		Declaration declaration = new Declaration(birthDate, pacs);
		return declaration;
	}

	public PAC createPAC(final String name, final Boolean over, final TypePac typePac) {
		return new PAC(name, over, typePac);

	}

	private Date createDate(final String sDate) throws ParseException {
		return DT.parse(sDate);
	}
}
